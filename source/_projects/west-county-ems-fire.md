---
name: West County EMS & Fire
full_name: West County EMS & Fire
length: 5 months
services:
    - Custom Website Development
    - Mobile Responsive Styling
    - Custom Software Development
    - Custom Database Development
    - 3rd Party API Integrations
main_image: /images/portfolio/fire-logo.jpg
thumbnail_image: /images/portfolio/fire-website-370x256.jpg
slideshow_images:
    - /images/portfolio/fire-website.jpg
    - /images/portfolio/fire-mobile.jpg
    - /images/portfolio/fire-website-1.jpg
    - /images/portfolio/fire-website-2.jpg
description: >
    West County EMS & Fire came to St. Louis Web Design to overhaul their digital presence and HR operations. St. Louis Web Design developed a custom front-end mobile responsive site and powerful web-based software to fulfill these needs.
---
<h4 class="h4-smaller">When West County EMS &amp; Fire first contacted St. Louis Web Design, they were in need of an updated web presence as well as updated systems to support their HR efforts.</h4>
<p>St. Louis Web Design developed a custom front-end website built on the Laravel platform. We also built secure admin software that helps West County EMS &amp; Fire streamline their entire HR department.</p>
