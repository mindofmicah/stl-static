---
name: Lou Fusz Company Admin
full_name: Lou Fusz Automotive Network
length: 4 months
services:
    - Custom Web Software
    - Database Development
    - 3rd Party API Integrations
main_image: /images/portfolio/fusz-logo.png
thumbnail_image: /images/portfolio/fusz-admin-370x256.jpg
slideshow_images:
    - /images/portfolio/fusz-admin-1700x909.jpg
    - /images/portfolio/fusz-admin-1700x909-1.jpg
    - /images/portfolio/fire-mobile.jpg
description: >
    This project was completed in the Laravel PHP framework and aims to streamline operations at Lou Fusz
---
<h4 class="h4-smaller">Lou Fusz came to us to build custom software to help streamline operations and communication with their close to 1,000 employees.</h4>
<p>We developed a custom database and software on top of the flexible, yet extremely powerful Laravel framework that helps streamline many of the day to day operations to help keep this industry leading dealer network fast and responsive.</p>
