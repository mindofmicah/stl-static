---
name: Elsevier
full_name: Elsevier
length: 6 months
services:
    - Custom Software Development
    - Custom Database Development
main_image: /images/portfolio/elsevier-logo.jpg
thumbnail_image: /images/portfolio/elsevier-370x256.jpg
slideshow_images:
    - /images/portfolio/elsevier-website.jpg
    - /images/portfolio/elsevier-mobile.jpg
description: >
    Elseiver is a billion dollar multi-national corporation that reached out to St. Louis Web Design to help them with their inventory management software system.
---
<h4 class="h4-smaller">Elsevier needed help optimizing their online inventory management system for their thousands of online medical publications. They deemed their systems were hard to use and un-optimized.</h4>
<p>St. Louis Web Design worked to understand their exact needs and criteria so that we could best address with their legacy system shortcomings.</p>
