---
name: Ackerman Fitness
full_name: Chicago Children's Museum
length: 4 months
services:
    - Custom Website Development
    - Website Hosting
    - Mobile Responsive Styling
main_image: /images/portfolio/ackerman-logo.jpg
thumbnail_image: /images/portfolio/ackerman-website-370x256.jpg
slideshow_images:
    - /images/portfolio/mens-clinic-website.jpg
    - /images/portfolio/ackerman-mobile.jpg
    - /images/portfolio/ackerman-website-1.jpg
    - /images/portfolio/ackerman-website-2.jpg
description: >
    The parent organization behind Ackerman Sports and Fitness Center had worked with us before. We were please when they came back to us regarding the site for their new state of the art fitness complex.
---
<h4 class="h4-smaller">Ackerman Sports and Fitness Center needed a website that showcased the many amenities of their facility, and also to help use that awareness to help drive up membership levels.</h4>
<p>After learning about their priorities and goals, we created them a website that was rich in media which helped showcase their facility. We then utilized that positive perception to help start visitors down the path of purchasing a membership.</p>
