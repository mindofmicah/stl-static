---
name: Dvorak Renovations
full_name: Dvorak Renovations
length: 3 months
services:
    - WordPress Website Development
    - Mobile Responsive Styling
main_image: /images/portfolio/dvorak-logo.jpg
thumbnail_image: /images/portfolio/dvorak-website-370x256.jpg
slideshow_images:
    - /images/portfolio/dvorak-website.jpg
    - /images/portfolio/dvorak-mobile.jpg
    - /images/portfolio/dvorak-website-1.jpg
description: >
    Dvorak Renovations came to St. Louis Web Design to partner on a new website that heavily highlights their past renovation works as well as their past customer testimonials.
---
<h4 class="h4-smaller">Dvorak Renovations came to St. Louis Web Design to partner on a website that not only attracted new visitors from search engines, but also converted them into leads based on the site's processional looks, and highlighting of their past successes.</h4>
<p>This system was built on top of the WordPress platform. This allows Dvorak Renovations to quickly and easily update the website content, including past project images, on their own without coding, as well as allowing them to easily deploy third party plugins when they need to add additional functionality in the future.</p>
