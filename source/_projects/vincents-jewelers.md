---
name: Vincent's Jewelers
full_name: Vincent's Jewelers
length: 4+ years
services:
    - Digital Marketing
    - Website Optimization
main_image: /images/portfolio/vj-logo.jpg
thumbnail_image: /images/portfolio/vj-website-370x256.jpg
slideshow_images:
    - /images/portfolio/vj-website.jpg
    - /images/portfolio/vj-mobile.jpg
    - /images/portfolio/vj-website-1.jpg
description: >
    Although Vincent's Jewelers was already one of the leading jewelers in the region, they wanted to increase the rate of their growth so they turned to St. Louis Web Design.
---
<p class="h4-smaller">St. Louis Web Design's partnership with Vincent's Jewelers has grown for the past 4 years due to the quality and profitable results it has produced. In fact Vincent's Jewelers has set sales record after sales record in part due to their cutting edge digital efforts.</p>
<p>St. Louis Web Design not only partners with Vincent's Jewelers on their digital paid advertising, but we also often discuss organic search optimization as well as overall marketing direction. We enjoy a true partnership and look forward to continuing to set records together.</p>
