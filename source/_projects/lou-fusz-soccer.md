---
name: Lou Fusz Soccer
full_name: Lou Fusz Parts
length: 3 months
services:
    - WordPress Website Development
    - Mobile Responsive Styling
    - Web Hosting
    - Ecommerce Integration
main_image: /images/portfolio/soccer-logo.jpg
thumbnail_image: /images/portfolio/soccer-website-370x256.jpg
slideshow_images:
    - /images/portfolio/soccer-website.jpg
    - /images/portfolio/soccer-mobile.jpg
    - /images/portfolio/soccer-website-1.jpg
description: >
    Lou Fusz Soccer came to St. Louis Web Design to improve their online communication with their thousands of players and parents within their organization. St. Louis Web Design delivered a system in which their administrators can update their own site and improve these communication mechanisms.
---
<h4 class="h4-smaller">Lou Fusz Soccer approached St. Louis Web Design to partner with them while developing an upgraded version of their website. The new site was based on the WordPress platform and allowed their staff to quickly update messaging on the site without the need for any coding knowledge.</h4>
<p>The updated site also featured ecommerce integration that allowed the thousands of soccer club members and their families to purchase jerseys and other branded merchandise which helped the organization increase revenue while streamlining fulfillment operations.</p>
