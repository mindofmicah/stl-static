---
name: Lou Fusz Automotive
full_name: Lou Fusz Automotive
length: 8+ Years
services:
    - Website Development
    - Software Development
    - Internet Marketing
main_image: /images/portfolio/fusz-logo.png
thumbnail_image: /images/portfolio/fusz-website-370x256.jpg
slideshow_images:
    - /images/portfolio/calibre-website.jpg
    - /images/portfolio/calibre-mobile.jpg
description: >
    St. Louis Web Design has been partnering with Lou Fusz Automotive to fulfill many of their custom digital needs for almost a decade. We have partnered with Lou Fusz on many websites, software projects, and digital marketing initiatives.
---
<h4 class="h4-smaller">Lou Fusz Automotive first approached St. Louis Web Design almost 10 years ago for a custom software project that kept track of their used car inventory. Since then, we have partnered with Lou Fusz on over 10 different digital projects.</h4>
<p>We have partnered with with Lou Fusz to create multiple custom software platforms, several custom websites, as well as helped power their digital marketing efforts. They come back to us time and time again due to our willingness to go the extra mile in our partnerships which lets them know they can count on us to achieve their digital goals.</p>
