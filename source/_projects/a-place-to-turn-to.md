---
name: A Place To Turn To
full_name: A Place To Turn To
length: 2 months
services:
    - WordPress Website Development
    - Mobile Responsive Styling
main_image: /images/portfolio/apttt-logo.jpg
thumbnail_image: /images/portfolio/apttt-website-370x256.jpg
slideshow_images:
    - /images/portfolio/apttt-website.jpg
    - /images/portfolio/apttt-mobile.jpg
    - /images/portfolio/apttt-website-1.jpg
description: >
    A Place To Turn To turned to St. Louis Web Design to help them increase their digital presence in order to better serve the families of patients undergoing long term medical care (such as Cancer treatment) in the city.
---
<h4 class="h4-smaller">A Place To Turn To helps the families of long term medical care patients get quality housing close to area hospitals. They came to St. Louis Web Design and asked us to create a digital system that showcases area properties and allows interested families to fill out an online application.</h4>
<p>This system was built on top of the WordPress platform. This allows A Place To Turn To to quickly and easily update the website content on their own without coding, as well as allowing them to easily deploy third party plugins when they need to add additional functionality in the future.</p>
