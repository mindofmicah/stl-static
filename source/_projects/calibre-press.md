---
name: Calibre Press
full_name: Calibre Press
length: 4 months
services:
    - WordPress Website Development
    - Mobile Responsive Styling
    - Custom Software Development
main_image: /images/portfolio/calibre-logo.jpg
thumbnail_image: /images/portfolio/calibre-website-370x256.jpg
slideshow_images:
    - /images/portfolio/calibre-website.jpg
    - /images/portfolio/calibre-mobile.jpg
    - /images/portfolio/calibre-website-1.jpg
description: >
    Calibre Press is a company dedicated to law enforcement training center that specializes in public, police interaction. St. Louis Web Design has partnered with them to ensure that their digital presence is capable of supporting their organization's goals.
---
<h4 class="h4-smaller">Calibre Press came to St. Louis Web Design to create a new WordPress site where visitors can read that latest news and trends in the law enforcement community, as well as the ability for these visitors to sign up for seminars, and buy learning material.</h4>
<p>The Calibre Press website is https secured, has ecommerce capabilities, as well as class/seminar sign up functionality. The Calibre Press site also has mobile responsive styling and the powerful WordPress back-end that allows Calibre Press staff to create new content and update their website without the need for coding.</p>
