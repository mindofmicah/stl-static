---
name: SIUE
full_name: SIUE STEM Center
length: 2 months
services:
    - WordPress Website
    - Mobile Responsive Styling
    - 3rd Party API Integrations
main_image: /images/portfolio/siue-logo.jpg
thumbnail_image: /images/portfolio/siue-website-370x256.jpg
slideshow_images:
    - /images/portfolio/siue-website.jpg
    - /images/portfolio/siue-website-1.jpg
    - /images/portfolio/siue-mobile.jpg
    - /images/portfolio/siue-website-2.jpg
    - /images/portfolio/siue-website-3.jpg
description: >
    This development project was completed using the WordPress platform to allow for easy content updates from employees with little coding experience.
---
<h4 class="h4-smaller">The technology and engineering experts at SIUE partnered with us to help them complete their new STEM Center website. This site supports the STEM Center which aims to expose youth to Science, Technology, Engineering, and Math.</h4>
<p>This system was designed from the ground up with the goal of allowing the SIUE STEM team to easily keep the public up to date with the latest news and projects undertaken by SIUE STEM without the need for coding.</p>
