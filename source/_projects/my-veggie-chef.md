---
name: My Veggie Chef
full_name: My Veggie Chef
length: 6 months
services:
    - Custom Laravel Development
    - Integrated Custom Administration Software
    - Mobile Responsive Styling
    - Custom Database Development
    - 3rd Party API Integrations
    - Ecommerce Integration
main_image: /images/portfolio/mvc-logo.jpg
thumbnail_image: /images/portfolio/mvc-website-370x256.jpg
slideshow_images:
    - /images/portfolio/mvc-website.jpg
    - /images/portfolio/mvc-mobile.png
    - /images/portfolio/mvc-website-1.jpg
    - /images/portfolio/mvc-website-2.jpg
description: >
    This custom Ecommerce project was built on the Laravel PHP Framework. This system was built to expand and securely power their operations as they expand.
---
<h4 class="h4-smaller">The My Veggie Chef team chose St. Louis Web Design to partner with and develop a custom website and administration software that fully supports their ever expanding business operations.</h4>
<p>The custom developed encrypted database securely stores customer and transactional information which helps the My Veggie Chef team streamline and optimize their organization.</p>
