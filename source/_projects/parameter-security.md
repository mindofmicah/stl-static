---
name: Parameter Security
full_name: Parameter Security
length: 4 months
services:
    - Custom Website Development
    - Mobile Responsive Styling
main_image: /images/portfolio/parameter-logo.jpg
thumbnail_image: /images/portfolio/parameter-website-370x256.jpg
slideshow_images:
    - /images/portfolio/parameter-website.jpg
    - /images/portfolio/parameter-mobile.jpg
    - /images/portfolio/parameter-website-1.jpg
description: >
    Parameter Security is a leading cyber security firm that reached out to St. Louis Web Design to help them revamp their online presence starting with their own website.
---
<h4 class="h4-smaller">We worked with Parameter Security to develop a new secure custom website that
outlines their services and showcases their past successes. We have worked with Parameter to ensure
their future clients can navigate to, and understand the important services Parameter offers.</h4>
<p>St. Louis Web Design developed the Parameter Security website to automatically detect the screen size
a user is browsing with and automatically optimize website content down to fit that particular
screen.</p>
