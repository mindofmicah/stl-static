---
name: Super Bright LEDs
full_name: Super Bright LEDs
length: 7+ Years
services:
    - Digital Marketing
    - Website Optimization
main_image: /images/portfolio/sbl-logo.jpg
thumbnail_image: /images/portfolio/sbl-website-370x256.jpg
slideshow_images:
    - /images/portfolio/apttt-website.jpg
    - /images/portfolio/apttt-mobile.jpg
description: >
    Super Bright LEDs is the leading provider of LED lighting online. We have been partnering with Super Bright LEDs for the better part of the last decade on their cutting edge digital efforts.
---
<p class="h4-smaller">St. Louis Web Design has been a long term digital partnership with Super Bright LEDs in an effort to help increase traffic to, and optimize their website. This relationship has persisted for so long due to the us helping them increase their traffic and revenues many times over.</p>
<p>Some of the many projects we undertake to help Super Bright LEDs are paid search advertising, display and remarketing, SEO, A/B testing, among others. Since working with us, Super Bright LEDs has continued to dominate the industry and have almost tripled the size of their company.</p>
