---
name: Back Care Clinic
full_name: Back Care Clinic
length: 2 months
services:
    - Custom Website Development
    - Mobile Responsive Styling
    - Custom Database Development
    - 3rd Party API Integrations
main_image: /images/portfolio/chiropractor-logo.jpg
thumbnail_image: /images/portfolio/chiropractor-website-370x256.jpg
slideshow_images:
    - /images/portfolio/chiropractor-website.jpg
    - /images/portfolio/chiropractor-mobile.jpg
    - /images/portfolio/chiropractor-website-1.jpg
    - /images/portfolio/chiropractor-website-2.jpg
description: >
    This site was developed to create additional exposure for a growing practice of physicians. It was designed to automatically optimize it's content for thousands of different screen sizes.
---
<h4 class="h4-smaller">The St. Louis Back Care Clinic needed a digital leg up on their competition so they turned to St. Louis Web Design to get the professional online exposure they needed to succeed.</h4>
<p>This website was developed utilizing html, css, and php. The website was styled so that for each visit it detects and automatically optimizes the site content for any device type.</p>
