---
name: Chicago Children's Museum
full_name: Chicago Children's Museum
length: 4 months
services:
    - Custom Website Development
    - Website Hosting
main_image: /images/portfolio/ccm-logo.jpg
thumbnail_image: /images/portfolio/ccm-website-370x256.jpg
slideshow_images:
    - /images/portfolio/mens-clinic-website.jpg
    - /images/portfolio/ccm-website-1.jpg
    - /images/portfolio/ccm-website-2.jpg
description: >
    The Chicago Children's Museum is an interactive museum on Chicago's Navy Pier that allows kids (and the entire family) to have fun while they explore, learn.
---
<h4 class="h4-smaller">The Chicago Children's Museum was started in 1995 has has grown to become one of the city's top attractions and the second largest children's museums in the country. The museum partnered with St. Louis Web Design to undertake their new website due to our reputation in the region. We took the time to understand their organizational goals before we even wrote a proposal.</h4>
<p>Because we took the time initially to totally understand their vision and goals for their online presence, Chicago Children's Museum received a website that exceeded their expectations and has helped them continue their growth.</p>
