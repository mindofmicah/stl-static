<div class="preloader">
    <div class="preloader-logo"><img src="/images/st-louis-web-design-logo-161x89.png" alt="" width="161" height="39" srcset="/images/logo-default-281x45.png 2x"/>
    </div>
    <div class="preloader-body">
        <div id="loadingProgressG">
            <div class="loadingProgressG" id="loadingProgressG_1"></div>
        </div>
    </div>
</div>
