<footer class="section footer-standard bg-gray-700">
    <!-- CTA Thin-->
    <section class="section section-xs bg-primary-darker text-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-10 col-md-12">
                    <div class="box-cta-thin">
                        <h4 style="margin-bottom: -0px;" class="wow-outer"><span  class="wow slideInRight">Get Started On Your Project <span class="font-weight-bold">Today</span></span></h4>
                        <div class="wow-outer button-outer"><a class="button button-primary button-winona wow slideInLeft" href="contact.php">Contact Us!</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End CTA Thin -->


    <div align="center" class="footer-standard-main">
        <div class="container">
            <div class="row row-50">
                <div class="col-lg-4">
                    <div class="inset-right-1">
                        <h4>Who We Are</h4>
                        <p>St. Louis Web Design is an award winning firm working with some of St. Louis's most admired organizations helping with website design, software development, web hosting, and internet marketing.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4">
                    <div class="box-1">
                        <h4>Get In Touch</h4>
                        <ul class="list-sm">
                            <li class="object-inline"><span class="icon icon-md mdi mdi-map-marker text-gray-700"></span><a class="link-default" href="#"><span itemprop="streetAddress">1720 Chouteau Ave Ste. 505</span><br> <span itemprop="addressLocality">St. Louis</span>, <span itemprop="addressRegion">MO</span> <span itemprop="postalCode">63103</span></a></li>
                            <li class="object-inline"><span class="icon icon-md mdi mdi-email text-gray-700"></span><a class="link-default" href="mailto:sales@stlouiswebdesign.com">sales@stlouiswebdesign.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-7 col-lg-4">
                    <h4>Shoot Us A Message</h4>
                    <p>We typically reply to new business client inquiries within one business day</p>
                    <div class="wow-outer button-outer"><a class="button button-primary button-winona wow slideInLeft" href="/contact.php">Contact Us!</a></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-standard-aside"><a class="brand" href="index.html"><img src="/images/st-louis-web-design-logo-light-161x89.png" alt="" width="161" height="89" srcset="images/st-louis-web-design-logo-light-281x156.png 2x"/></a>
            <!-- Rights-->
            <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>All Rights Reserved.</span><span>&nbsp;</span><br class="d-sm-none"/><a href="terms.php">Terms of Use</a><span> and</span><span>&nbsp;</span><a href="privacy.php">Privacy Policy</a></p>
        </div>
    </div>
        <!--Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5b72270af31d0f771d83c210/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--/Tawk.to Script-->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2640992-18"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-2640992-18');
        </script>

        <!-- /Global site tag (gtag.js) - Google Analytics -->
</footer>
