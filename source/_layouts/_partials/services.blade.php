<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <h3 class="wow-outer"><span class="wow slideInUp">What Can We Help With?</span></h3>
        <div class="row row-30">
            @foreach($services as $service)
            <div class="col-sm-6 col-lg-4 wow-outer">
                <!-- Box Creative-->
                <article class="box-creative wow fadeInLeft" data-wow-delay=".{{($loop->iteration % 3)}}s">
                    <a href="{{$service->getUrl()}}">
                    <div class="box-creative-icon {{$service->icon}}"></div>
                    <h4 class="box-creative-title">{{$service->name}}
				</a></h4>
                <p>{{$service->excerpt}}</p>
                </article>
            </div>
            @endforeach
        </div>
    </div>
</section>
