<?php 
$collection = '';
if($page instanceOf \TightenCo\Jigsaw\Collection\CollectionItem) {
    $collection = $page->getCollection();
}
?>
<header class="section page-header">
    <!-- RD Navbar-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-minimal" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-fixed" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-main-outer">
                <div class="rd-navbar-main">
                    <!-- RD Navbar Panel-->
                    <div class="rd-navbar-panel">
                        <!-- RD Navbar Toggle-->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle="#rd-navbar-nav-wrap-1"><span></span></button>
                        <!-- RD Navbar Brand--><a class="rd-navbar-brand" href="index.php"><img src="/images/st-louis-web-design-logo-161x89.png" alt="St. Louis Web Design Logo" width="161" height="89"/></a>
                    </div>
                    <div class="rd-navbar-main-element">
                        <div class="rd-navbar-nav-wrap" id="rd-navbar-nav-wrap-1">
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                <li class="<?php if(isset($home_page)){echo $home_page;}else{echo "rd-nav-item";} ?>"><a class="rd-nav-link" href="index.php">Home</a>
                                </li>
                                <li class="rd-nav-item {{($collection == 'services' ? 'active':'')}}"><a class="rd-nav-link" href="#">Services</a>
                                                  <!-- Portfolio Navbar Dropdown-->
                                    <ul class="rd-menu rd-navbar-dropdown">
                                        @foreach($services as $service)

                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="{{$service->getUrl()}}">{{$service->name}}</a></li>
                                        @endforeach
                                    </ul>
								</li>
                                <li class="rd-nav-item {{($collection == 'projects' ? 'active' : '')}}"><a class="rd-nav-link" href="{{$projects->first()->base_url}}">Portfolio</a>
                                    <!-- Portfolio Navbar Dropdown
                                    <ul class="rd-menu rd-navbar-dropdown">
                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="grid-layout.php">Grid Layout</a></li>
                                        <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="single-project.php">Single Project</a></li>
                                    </ul>
									--/Portfolio Navbar Dropdown-->
                                </li>

                                <li class="<?php if(isset($contact_page)){echo $contact_page;}else{echo "rd-nav-item";} ?>"><a class="rd-nav-link" href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                        <!-- RD Navbar Search-->
                        <!--
                        <div class="rd-navbar-search" id="rd-navbar-search-1">
                            <button class="rd-navbar-search-toggle rd-navbar-fixed-element-2" data-rd-navbar-toggle="#rd-navbar-search-1"><span></span></button>
                            <form class="rd-search" action="search-results.php" data-search-live="rd-search-results-live-1" method="GET">
                                <div class="form-wrap">
                                    <label class="form-label" for="rd-navbar-search-form-input-1">Search...</label>
                                    <input class="form-input rd-navbar-search-form-input" id="rd-navbar-search-form-input-1" type="text" name="s" autocomplete="off">
                                    <div class="rd-search-results-live" id="rd-search-results-live-1"></div>
                                </div>
                                <button class="rd-search-form-submit fa-search" type="submit"></button>
                            </form>
                        </div>
                        -->
                        <img src="/images/partnerbadge.png" alt="award winning partner">

                    </div>
                </div>
            </div>
        </nav>
    </div>
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">

</header>
