<!-- Stylesheets-->
<link rel="stylesheet" href="/css/style.css" id="main-styles-link">

<!--
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/fonts.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link rel="stylesheet" href="css/animated-headlines.css">
<link href="css/lightbox.min.css" rel="stylesheet">
<link href="css/flexslider.css" rel="stylesheet">
-->

<script type="text/javascript">
/* First CSS File */
var giftofspeed = document.createElement('link');
giftofspeed.rel = 'stylesheet';
giftofspeed.href = '/css/bootstrap.css';
giftofspeed.type = 'text/css';
var godefer = document.getElementsByTagName('link')[0];
godefer.parentNode.insertBefore(giftofspeed, godefer);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = '/css/fonts.css';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = '/css/style.css';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = 'https://use.fontawesome.com/releases/v5.2.0/css/all.css';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = '/css/animated-headlines.css';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = '/css/lightbox.min.css';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = '/css/flexslider.css';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

/* Second CSS File */
var giftofspeed2 = document.createElement('link');
giftofspeed2.rel = 'stylesheet';
giftofspeed2.href = '//fonts.googleapis.com/css?family=Work+Sans:300,700,800%7COswald:300,400,500';
giftofspeed2.type = 'text/css';
var godefer2 = document.getElementsByTagName('link')[0];
godefer2.parentNode.insertBefore(giftofspeed2, godefer2);
</script>

<noscript>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Work+Sans:300,700,800%7COswald:300,400,500">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/fonts.css">
<link rel="stylesheet" href="css/style.css" id="main-styles-link">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link rel="stylesheet" href="css/animated-headlines.css">
<link href="css/lightbox.min.css" rel="stylesheet">
<link href="css/flexslider.css" rel="stylesheet">
</noscript>


<!--<link href="css/featherlight.min.css" rel="stylesheet">-->
<!--<link href="css/flexslider-rtl.css" rel="stylesheet">-->

<!--[if lt IE 10]>
<div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
<script src="js/html5shiv.min.js"></script>
<![endif]-->
