<section class="section section-lg bg-gray-100 text-center">
    <div class="container">
        <h3>Testimonials</h3>
        <div class="slick-widget-testimonials wow fadeIn">
            <div class="slick-slider carousel-child" id="child-carousel" data-for=".carousel-parent" data-arrows="true" data-loop="true" data-dots="false" data-swipe="true" data-items="1" data-sm-items="3" data-md-items="5" data-lg-items="5" data-xl-items="5" data-center-mode="true" data-slide-to-scroll="1">
                @foreach($testimonials as $testimonial)
                <div class="item wow-outer">
                    <img class="wow slideInLeft" src="{{$testimonial->image}}" alt="" width="96" height="96"/>
                </div>
                @endforeach
            </div>
            <!-- Slick Carousel-->
            <div class="slick-slider carousel-parent" data-arrows="false" data-loop="true" data-dots="false" data-swipe="false" data-items="1" data-fade="true" data-child="#child-carousel" data-for="#child-carousel">
                @foreach($testimonials as $testimonial)
                <div class="item">
                    <!-- Quote Light 1-->
                    <blockquote class="quote-light">
                        <cite class="quote-light-cite">{{$testimonial->name}}</cite>
                        <p class="quote-light-caption">{{$testimonial->title}}</p>
                        <svg class="quote-light-mark" x="0px" y="0px" width="35px" height="25px" viewbox="0 0 35 25">
                            <path d="M27.461,10.206h7.5v15h-15v-15L25,0.127h7.5L27.461,10.206z M7.539,10.206h7.5v15h-15v-15L4.961,0.127h7.5                L7.539,10.206z"></path>
                        </svg>
                        
                        <div class="quote-light-text">
                            {!!$testimonial->getContent()!!}
                        </div>
                    </blockquote>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
