<?php
$portfolio_page = "rd-nav-item active"
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
      <title>@yield('title', $page->title())</title>
    <meta name="description" content="St. Louis Web Design Client - SIUE">
    <meta name="author" content="St Louis Web Design Company">
    <meta content="INDEX, FOLLOW" name="GOOGLEBOT">
    <meta name="distribution" content="Global">
    <meta name="rating" content="Family">
    <meta name="language" content="English">
    <meta name="revisit-after" content="7 days">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width height=device-height initial-scale=1.0 maximum-scale=1.0 user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- CSS StyleSheets -->
    @include('_layouts._partials.styles')
      <link href="/css/featherlight.min.css" rel="stylesheet">
    <!-- End CSS Stylesheets -->
  </head>
  <body>
      @yield('contents')
      <!-- Footer-->
      @include('_layouts._partials.preloader')
      @include('_layouts._partials.footer')

      <!-- Javascript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>


    @include('_layouts._partials.javascript')
    <script src="/js/jquery.flexslider.js"></script>
    <script src="/js/jquery.flexslider-min.js"></script>
    <script src="/assets/js/main.js"></script>

    <script>
        // Can also be used with $(document).ready()
        $(window).load(function() {
            $('.flexslider').flexslider({
                animation: "slide"
            });
        });
    </script>
  </body>
  <html>
