@extends('_layouts.master')

@section('contents')

    <div class="page">
      <!-- Page Header & Main Nav-->
      @include('_layouts._partials.main_nav')
      <!-- End Page Header & Main Nav -->
      <!-- Breadcrumbs -->
      <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(/images/portfolio-client-header.jpg);">
        <div class="breadcrumbs-custom-inner">
          <div class="container breadcrumbs-custom-container">
            <div class="breadcrumbs-custom-main">
              <h6 class="breadcrumbs-custom-subtitle title-decorated">Portfolio</h6>
              <h1 class="breadcrumbs-custom-title">{{$page->name}}</h1>
            </div>
            <ul class="breadcrumbs-custom-path">
              <li><a href="/">Home</a></li>
              <li><a href="st-louis-web-design-portfolio.php">Portfolio</a></li>
              <li class="active">{{$page->name}}</li>
            </ul>
          </div>
        </div>
      </section>
      <!-- Overview-->
      <section class="section section-sm bg-gray-100">
        <div class="container">
          <!-- Owl Carousel-->
          <div align="center" class="owl-carousel owl-carousel-centered-pagination" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="4" data-dots="true" data-stage-padding="0" data-loop="false" data-margin="30" data-mouse-drag="false">
            <div class="item">
              <h4>Client</h4>
              <p>{{$page->full_name}}</p>
            </div>
            <div class="item">
              <h4>Services</h4>
              <ul class="list-inline-comma list-inline-comma-default">
                  @foreach($page->services as $service)
                  <li>{{$service}}</li>
                  @endforeach
              </ul>
            </div>
            <div class="item">
              <h4>Brief Description</h4>
              <p><span style="max-width: 240px;">{{$page->description}}</span></p>
            </div>
            <div class="item">
              <h4>Project Length</h4>
              <p>{{$page->length}}</p>
            </div>
          </div>
        </div>
      </section>
      <!-- About This Project -->

      <section class="section">
        <div class="container">
          <div align="center">
              <img src="{{$page->main_image}}" alt="{{$page->name}} logo" />
          </div>
        </div>
      </section>


                <div align="center">
                  <div style="width: 100%; background-color: #f5f6fa;">
                  <div style="padding-top: 10px; padding-bottom: 10px; max-width: 1500px;">
                <h3>About This Project</h3>
                @yield('content')
                <a class="button button-primary button-winona" href="contact.php" style="min-width: 230px;">Start Your Project Today!</a>
                    </div>
                  </div>


          </div>
        <div id="main" role="main">
        <section class="slider">
    <div class="flexslider" id="f1" style="direction:ltr">
        <ul class="slides">
            @foreach($page->slideshow_images as $index => $image)
            <li>
                <a href="https://www.altecdesign.com/dev1{{$image}}" data-lightbox="{{$index + 1}}" data-title="My caption"><img src="{{$image}}" /></a>
            </li>
            @endforeach
          <li>
        </ul>
    </div>
    </section>
        </div>
    </div>


    <!-- Services-->
      @include('_layouts._partials.services')
    <!-- End Services -->


      <!-- Testimonials Type 2-->
      @include('_layouts._partials.testimonials-2')
      <!-- End Testimonials Type 2-->


      <!-- Preloader -->
      <!-- End Preloader -->

      <!-- PANEL-->
@endsection
