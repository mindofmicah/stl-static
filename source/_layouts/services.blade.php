@extends('_layouts.master')

@section('contents')

    <div class="page">
      <!-- Page Header & Main Nav-->
      @include('_layouts._partials.main_nav')
      <!-- End Page Header & Main Nav -->
      <!-- Breadcrumbs -->
      <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(images/web-design-banner.jpg);">
        <div class="breadcrumbs-custom-inner">
          <div class="container breadcrumbs-custom-container">
            <div class="breadcrumbs-custom-main">
              <h6 class="breadcrumbs-custom-subtitle title-decorated">services</h6>
              <h1 class="breadcrumbs-custom-title">Web Design</h1>
            </div>
            <ul class="breadcrumbs-custom-path">
              <li><a href="index.html">Home</a></li>
              <li class="active">Web Design</li>
            </ul>
          </div>
        </div>
      </section>
      <!-- A Few Words About Us-->
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <div class="row row-50 justify-content-center justify-content-lg-between">
            <div class="col-md-10 col-lg-6 col-xl-5">
                <h3>{{$page->name}} Services</h3>
                <p>{{$page->description}}</p>
              <div>
                  <a class="button button-lg button-primary button-winona" href="/contact.php">Ready For A Change?</a>
              </div>
            </div>
            <div class="col-md-10 col-lg-6"><img class="img-responsive" src="{{$page->image}}" alt="web design meeting"/>
            </div>
          </div>
        </div>
      </section>
		<div style="padding-top: 35px;">
	      <div class="container wow fadeInRight cont-container" style="margin-bottom: 35px;" >
          <div class="justify-content-center justify-content-lg-between" style="padding-bottom:20px">

              <h3 style="padding-top:20px; text-decoration: underline;">Why Choose St. Louis Web Design?</h3>
              <p>{{$page->pitch}}</p>


        @yield('content')
	  
	  
      @include('_layouts._partials.services')

      <!-- Call To Action -->
      <section class="section section-xl bg-gray-700 bg-image bg-image-1" style="background-image: url(/images/call-to-action-arch-1920x584.jpg);">
        <div class="container">
          <div class="row justify-content-sm-end">
            <div class="transbox col-sm-9 col-md-7 col-lg-6">
              <div align="center">
                <div class="wow-outer">
                  <div class="wow slideInUp">
                    <h4>Are You Ready For A Change?</h4>
                    <h3 class="font-weight-bold">Take The First Step Towards Success</h3>
                  </div>
                </div>
                <div class="wow-outer offset-top-4">
                  <div class="wow slideInDown">
                    <p>Learn about how you can leverage our digital capabilities to achieve further success.</p><a class="button button-primary button-winona" href="contact.php">Schedule A Free Consultation</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
          </div></div>


      <!-- Testimonials Type 2-->
      @include('_layouts._partials.testimonials-2')
      <!-- End Testimonials Type 2-->


@endsection
