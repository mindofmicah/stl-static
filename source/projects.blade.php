@extends('_layouts.master')
@section('contents')
    <div class="page">
      <!-- Page Header & Main Nav-->
      <?php include 'main_nav.php'; ?>
      <!-- End Page Header & Main Nav -->
      <!-- Breadcrumbs -->
      <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(/images/portfolio-header.jpg);">
        <div class="breadcrumbs-custom-inner">
          <div class="container breadcrumbs-custom-container">
            <div class="breadcrumbs-custom-main">
              <!--<h6 class="breadcrumbs-custom-subtitle title-decorated">Portfolio</h6>-->
              <h1 class="breadcrumbs-custom-title">Portfolio</h1>
            </div>
            <ul class="breadcrumbs-custom-path">
              <li><a href="index.html">Home</a></li>
              <li><a href="#">Portfolio</a></li>
            </ul>
          </div>
        </div>
      </section>
      <section class="section section-lg oh">
        <div class="container">
            <h4 align="center">Below you can find a few of our recent projects.</h4>
          <!-- Isotope Filters-->
          <div class="isotope-filters isotope-filters-modern">
            <button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
            <ul class="isotope-filters-list" id="isotope-filters">
              <li><a class="active" data-isotope-filter="*" data-isotope-group="portfolio" href="#">All Types</a></li>
              @foreach($projects->first()->filterable_services as $service)

              <li><a data-isotope-filter="{{$service}}" data-isotope-group="portfolio" href="#">{{$service}}</a></li>
              @endforeach
            </ul>
          </div>
          <div class="isotope isotope-responsive row" data-isotope-layout="fitRows" data-isotope-group="portfolio">
              <?php foreach($projects as $project):?>

              <div class="col-sm-6 col-lg-4 isotope-item" data-filter="{{$project->filteredServices()->implode(', ')}}">
                <!-- Thumbnail Classic--><a class="thumbnail-classic thumbnail-classic-sm" href="{{$project->getUrl()}}"><img class="thumbnail-classic-image" src="{{$project->thumbnail_image}}" alt="{{$project->name}} website" width="370" height="256"/>
                <div class="thumbnail-classic-caption">
                    <p class="thumbnail-classic-title">{{$project->full_name}}</p>
                    <p class="thumbnail-classic-text">{{implode(', ', $project->services)}}</p>
                </div>
                <div class="thumbnail-classic-dummy"></div></a>
            </div>
            <?php endforeach;?>
              </div>
          </div>
        </div>
      </section>
@endsection
