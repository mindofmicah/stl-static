$(function() {
	var $scholarship = $('#scholarship');
	$('.header-bar', $scholarship).click(function() {
		$('.dropdown', $scholarship).slideToggle();
	});

	if (window.location.hash === '#web-scholarship') {
		$('.dropdown', $scholarship).show();
		(window.pageTransitionCallbacks = window.pageTransitionCallbacks || []).push(function() {
			$('.top-head').addClass('sticky');
			$('html, body').animate({
				scrollTop: $scholarship.offset().top - $('.rd-navbar').outerHeight()
			});
		});
	}

	$('#scholarship form').parsley();

	$('#scholarship form').submit(function() {
		$(this).append('<input type="hidden" name="prevent_spam" value="prevent_spam">');
	});
});