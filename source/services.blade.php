@extends('_layouts.master')

@section('contents')

<?php
$services_page = "rd-nav-item active"
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Our Services </title>
    <meta name="description" content="St Louis Web Design Company provides powerful internet solutions to the most admired companies in St. Louis MO. Web Design / Custom Software / Web Marketing">
    <meta name="keywords" content="Web Design St. Louis, St. Louis Web Design, St. Louis Website Design, Website Design St. Louis, St. Louis PPC, PPC St. Louis, SEO St. Louis, St, Louis SEO">
    <meta name="author" content="St Louis Web Design Company">
    <meta content="INDEX, FOLLOW" name="GOOGLEBOT">
    <meta name="distribution" content="Global">
    <meta name="rating" content="Family">
    <meta name="language" content="English">
    <meta name="revisit-after" content="7 days">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width height=device-height initial-scale=1.0 maximum-scale=1.0 user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <!-- Favicon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- CSS StyleSheets -->
    <?php include 'styles.php'; ?>
    <!-- End CSS Stylesheets -->

  </head>
  <body>
    <div class="page">
      <!-- Page Header & Main Nav-->
      <?php include 'main_nav.php'; ?>
      <!-- End Page Header & Main Nav -->
      <!-- Breadcrumbs -->
      <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(images/breadcrumbs-image-1.jpg);">
        <div class="breadcrumbs-custom-inner">
          <div class="container breadcrumbs-custom-container">
            <div class="breadcrumbs-custom-main">
              <h6 class="breadcrumbs-custom-subtitle title-decorated">Services</h6>
              <h1 class="breadcrumbs-custom-title">Services</h1>
            </div>
            <ul class="breadcrumbs-custom-path">
              <li><a href="index.html">Home</a></li>
              <li class="active">Services</li>
            </ul>
          </div>
        </div>
      </section>
      <!-- A Few Words About Us-->
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <div class="row row-50 justify-content-center justify-content-lg-between">
            <div class="col-md-10 col-lg-6 col-xl-5">
              <h3>A Few Words About Us</h3>
              <p>We are a team of talented marketers who happen to love creating smart ideas for those who will have us. We use our creative potential to provide the smartest ideas.</p>
              <p>We have a wide range of experience, expertise and tools to create and implement your campaigns, from carefully curating awesome content to optimising it with our great SEO powers as well as outdoor marketing skills.</p><a class="button button-lg button-primary button-winona" href="about-us.html">Learn more</a>
            </div>
            <div class="col-md-10 col-lg-6"><img class="img-responsive" src="images/services-1-570x368.jpg" alt="" width="570" height="368"/>
            </div>
          </div>
        </div>
      </section>
      <!-- Services-->
      <?php include 'services.php'; ?>
      <!-- End Services -->

      <!-- Call To Action -->
      <section class="section section-xl bg-gray-700 bg-image bg-image-1" style="background-image: url(images/call-to-action-2-1920x584.jpg);">
        <div class="container">
          <div class="row justify-content-sm-end">
            <div class="col-sm-9 col-md-7 col-lg-6">
              <div class="box-2">
                <div class="wow-outer">
                  <div class="wow slideInUp">
                    <h4>Take the First Step to the</h4>
                    <h3 class="font-weight-bold">Success of Your Company</h3>
                  </div>
                </div>
                <div class="wow-outer offset-top-4">
                  <div class="wow slideInDown">
                    <p>We can make your company the leader of the market by providing high quality business management solutions. Schedule an appointment to find out more.</p><a class="button button-primary button-winona" href="#">Make an Appointment</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End Call To Action -->

      <!-- Testimonials-->
      <?php include 'testimonials.php'; ?>
      <!-- End Testimonials -->

      <!-- Footer-->
      <?php include 'footer.php'; ?>
      <!-- End Footer -->

      <!-- Preloader -->
      <?php include 'preloader.php'; ?>
      <!-- End Preloader -->

    <!-- PANEL-->
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <?php include 'javascript.php'; ?>
    <!-- End Javascript -->
  </body>
</html>
@endsection
