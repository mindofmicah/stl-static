<?php
return [
    'baseUrl' => '',
    'production' => false,
    'title' => function () {
        return 'St. Louis Web Design';
    },
    'collections' => [
        'services'=> [
            'path'    => 'services/{-name}',
            'extends' => '_layouts.services',
            'section' => 'content',
        ],
        'testimonials'=>[
        
        ],
        'projects'=> [
            'base_url'=>'/projects',
            'filterable_services' => [
                'WordPress Website' => 'WordPress',
                'Website Hosting' => 'Hosting',
                'Mobile Responsive Styling' => 'Responsive Design',
                '3rd Party API Integrations' => '3rd-party',
            ],
            'filteredServices' => function ($page) {
                return $page->filterable_services->only($page->services)->values();
            },
            'path'    => 'projects',
            'extends' => '_layouts.projects',
            'section' => 'content',
            'title' => function ($page) {
                return $page->page_title ?? $page->name . ' Web Project';
            }
        ]
    ],
];
