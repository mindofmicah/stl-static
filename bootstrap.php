<?php

use TightenCo\Jigsaw\Jigsaw;

/** @var $container \Illuminate\Container\Container */
/** @var $events \TightenCo\Jigsaw\Events\EventBus */

/**
 * You can run custom code at different stages of the build process by
 * listening to the 'beforeBuild', 'afterCollections', and 'afterBuild' events.
 *
 * For example:
 *
 * $events->beforeBuild(function (Jigsaw $jigsaw) {
 *     // Your code here
 * });
 */
$events->afterBuild(function (Jigsaw $jigsaw) {
    $stuff = [
        'services' => [
            'name',
            'excerpt',
        ],
        'projects' => [
            'name', 
            'description',
            'full_name'
        ]
    ];

    $output = [];
    foreach ($jigsaw->getCollections() as $name => $collection) {
        foreach ($collection as $item) {
            $output[] = $item->only($stuff[$name] ?? [])->toArray();
        }
    }
    $output = array_values(array_filter($output));

    $jigsaw->writeOutputFile('search.json', json_encode($output));
});
